﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileApplication.Globals
{
    public class Global
    {
        //public const string baseUrl = "http://147.174.184.62:58198/";
        public const string baseUrl = "http://192.168.2.5:58198/";
        public const string apiProduct = "api/Products/";
		public const string apiManufacturer = "api/Manufacturers/";
		public const string apiCategories = "api/Categories/";
		public const string apiApiKey = "api/ApiKey?email=";
		public const string passwordQuery = "&password=";
    }
}
