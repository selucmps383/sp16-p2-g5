﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApplication.Models;
using Xamarin.Forms;

namespace MobileApplication.Pages
{
    public class TabsPage : TabbedPage
    {
        private readonly Page tab1Page;
        private readonly Page tab2Page;
        private readonly Page tab3Page;

        public TabsPage(User user)
        {
            Title = "383 Management App";
            tab1Page = new ProductsPage(user) { Title = "Products" };
            tab2Page = new CategoryPage(user) { Title = "Categories" };
            tab3Page = new ManufacturerPage(user) { Title = "Manufacturers" };

            Children.Add(tab1Page);
            Children.Add(tab2Page);
            Children.Add(tab3Page);
        } 

        public void SwitchToTab1()
        {
            CurrentPage = tab1Page;
        }

        public void SwitchToTab2()
        {
            CurrentPage = tab2Page;
        }

        public void SwitchToTab3()
        {
            CurrentPage = tab3Page;
        }
    }
}
