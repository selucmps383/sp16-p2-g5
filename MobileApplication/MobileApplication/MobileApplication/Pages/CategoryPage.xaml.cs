﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MobileApplication.Models;
using MobileApplication.Services;
using RestSharp.Portable;
using Xamarin.Forms;


namespace MobileApplication.Pages
{
	public partial class CategoryPage : ContentPage
	{
		private ListView listView;
	    private User _user;
	    

		protected override async void OnAppearing()
		{
            var request = new Rest(Globals.Global.apiCategories, Method.GET);
            var response = await SingletonClient.GetClient().Execute<ICollection<Category>>(request.request);

		    foreach (var VARIABLE in response.Data)
		    {
		        Application.Current.Properties.Add(VARIABLE.CategoryName, VARIABLE);
		    }
		   await Application.Current.SavePropertiesAsync();

            listView.ItemsSource = response.Data;
			base.OnAppearing();
		}

		public CategoryPage(User user)
		{
		    this._user = user;
			var createCategory = new Button()
			{ WidthRequest = 50, HeightRequest = 80, BackgroundColor = Color.FromHex("77D065"), Text = "Create New Category", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)) };
			createCategory.Clicked += CreateCategoryOnClicked;
			Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

			listView = new ListView
			{
				HasUnevenRows = true,
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.
						var nameLabel = new Label() { HeightRequest = 20 };
						nameLabel.SetBinding(Label.TextProperty, "CategoryName");
						nameLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command<Label>(OnLabelClicked), CommandParameter = nameLabel, NumberOfTapsRequired = 1 });



						return new ViewCell
						{
							View = new StackLayout
							{
								VerticalOptions = LayoutOptions.Center,
								Spacing = -7,
								Children =
								{
									new Label() { HeightRequest = 20},
									nameLabel,
                                    new Label() { HeightRequest = 20},
                                    new Label() { HeightRequest = 20},
                                    new Label() {HeightRequest = 30}

								}
								}
						};
					})
			};


			this.Content = new StackLayout
			{
				Children =
				{
					new Label() {HeightRequest = 20},
					listView,
					createCategory
				}
				};
		}

		async void OnLabelClicked(Label clickedLabel)
		{
			var temp = clickedLabel.BindingContext as Category;

			await Navigation.PushAsync(new CategoryManagementPage(temp, _user));
		}

		async void CreateCategoryOnClicked(object sender, EventArgs eventArgs)
		{
			await Navigation.PushAsync(new CategoryCreation(_user));
		}
	}
}

