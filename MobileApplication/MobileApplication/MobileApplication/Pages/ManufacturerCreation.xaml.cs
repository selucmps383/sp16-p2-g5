﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApplication.Models;
using MobileApplication.Services;
using RestSharp.Portable;
using Xamarin.Forms;

namespace MobileApplication.Pages
{
    public partial class ManufacturerCreation : ContentPage
    {
        private Manufacturer _manufacturer = new Manufacturer();
        private User _user;

        public ManufacturerCreation(User user)
        {
            this._user = user;
            this.BindingContext = _manufacturer;

            var nameLabel = new Label() {Text = "Manufacturer Name: "};
            var nameEntry = new Entry() {HorizontalOptions = LayoutOptions.FillAndExpand};
            nameEntry.SetBinding(Entry.TextProperty, "ManfacturerName");
            nameEntry.TextChanged += (sender, args) => { _manufacturer.ManfacturerName = args.NewTextValue; };
            var createButton = new Button() {Text = "Add a Manufacturer", HeightRequest = 50, WidthRequest = 80, BackgroundColor = Color.FromHex("77D065") };
            createButton.Clicked += CreateButtonOnClicked;

            var grid = new Grid()

            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                    new RowDefinition() {Height = new GridLength(50)},
                    new RowDefinition() {Height = new GridLength(50)}

                },
                ColumnDefinitions =
                {
                    new ColumnDefinition() {Width = GridLength.Auto},
                    new ColumnDefinition() {Width = new GridLength(200)}

                }
            };
            grid.Children.Add(nameLabel, 0, 1);
            grid.Children.Add(nameEntry, 1, 1);

            this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

            this.Content = new StackLayout() {Children = {grid, createButton}};

        }

        async void CreateButtonOnClicked(object sender, EventArgs eventArgs)
        {

            var request = new Rest(Globals.Global.apiManufacturer, Method.POST);
            var response = await SingletonClient.GetClient().Execute(request.request.AddJsonBody(_manufacturer));
            if (response.IsSuccess)
            {
                await Navigation.PushAsync(new TabsPage(_user));
            }
        }
    }
}
