﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MobileApplication.Annotations;
using MobileApplication.Models;
using MobileApplication.Pages;
using MobileApplication.Services;
using RestSharp.Portable;
using Xamarin.Forms;


namespace MobileApplication
{
	public partial class CategoryManagementPage : ContentPage
	{
		private Category _category;
	    private User _user;

		public CategoryManagementPage(Category category, User user)
		{
		    this._user = user;
			this._category = category;
			this.BindingContext = category;

			var nameLabel = new Label() { Text = "Category Name: "};
			var name = new Entry(){HorizontalOptions = LayoutOptions.FillAndExpand};
			name.SetBinding(Entry.TextProperty, "CategoryName");
			var deleteButton = new Button();
			deleteButton = new Button() {Text = "Delete", BackgroundColor = Color.FromHex("FF0000") };
			deleteButton.Clicked += DeleteButtonOnClicked;
			var saveButton = new Button() {Text = "Edit", BackgroundColor = Color.FromHex("FFFF00"), TextColor = Color.Black};
			saveButton.Clicked += SaveButtonOnClicked;

			var grid = new Grid()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions =
				{
					new RowDefinition() {Height = GridLength.Auto},
					new RowDefinition() {Height = GridLength.Auto},
                    
                    },
				ColumnDefinitions =
				{
					new ColumnDefinition() {Width = GridLength.Auto},
					new ColumnDefinition() {Width = new GridLength(200)},
					
				}
				};
			grid.Children.Add(nameLabel,0,2);
			grid.Children.Add(name,1,2);
			grid.Children.Add(deleteButton,0,8);
			grid.Children.Add(saveButton,1,8);

			this.Padding = new Thickness(10, Device.OnPlatform(20,0,0), 10, 5);

			this.Content = grid;
		}

		async void DeleteButtonOnClicked(object sender, EventArgs eventArgs)
		{
			var request = new Rest(Globals.Global.apiCategories + _category.CategoryId, Method.DELETE);
			var response = await SingletonClient.GetClient().Execute(request.request);
			if (response.IsSuccess)
			{
			    await Navigation.PushAsync(new TabsPage(_user));

			}
		}


		async void SaveButtonOnClicked(object sender, EventArgs eventArgs)
		{
			var request = new Rest(Globals.Global.apiCategories + _category.CategoryId, Method.PUT);

			var response = await SingletonClient.GetClient().Execute(request.request.AddBody(_category));
			if (response.IsSuccess)
			{
				await Navigation.PushAsync(new TabsPage(_user));
			}


		}
	}
}

