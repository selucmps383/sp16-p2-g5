﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApplication.Models;
using MobileApplication.Services;
using RestSharp.Portable;
using Xamarin.Forms;

namespace MobileApplication.Pages
{
    public partial class ManufacturerDetails : ContentPage
    {
        private Manufacturer _manufacturer;
        private User _user;

        public ManufacturerDetails(Manufacturer manufacturer, User user)
        {
            this._user = user;
            this._manufacturer = manufacturer;
            this.BindingContext = manufacturer;

            var nameLabel = new Label() { Text = "Manufacturer Name: " };
            var name = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand };
            name.SetBinding(Entry.TextProperty, "ManfacturerName");

            var deleteButton = new Button();
            deleteButton = new Button() { Text = "Delete", BackgroundColor = Color.FromHex("FF0000") };
            deleteButton.Clicked += DeleteButtonOnClicked;
            var saveButton = new Button() { Text = "Edit", BackgroundColor = Color.FromHex("FFFF00"), TextColor = Color.Black };
            saveButton.Clicked += SaveButtonOnClicked;

            var grid = new Grid()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions =
                {
                    new RowDefinition() {Height = GridLength.Auto},
                    new RowDefinition() {Height = GridLength.Auto}
                    
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition() {Width = GridLength.Auto},
                    new ColumnDefinition() {Width = new GridLength(200)},
                }
            };
            grid.Children.Add(nameLabel, 0, 2);
            grid.Children.Add(name, 1, 2);
            grid.Children.Add(deleteButton, 0, 8);
            grid.Children.Add(saveButton, 1, 8);

            this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

            this.Content = grid;
        }

        async void DeleteButtonOnClicked(object sender, EventArgs eventArgs)
        {
            var request = new Rest(Globals.Global.apiManufacturer + _manufacturer.ManufacturerId, Method.DELETE);
            var response = await SingletonClient.GetClient().Execute(request.request);
            if (response.IsSuccess)
            {
                await Navigation.PushAsync(new TabsPage(_user));
            }
        }


        async void SaveButtonOnClicked(object sender, EventArgs eventArgs)
        {
            var request = new Rest(Globals.Global.apiManufacturer + _manufacturer.ManufacturerId, Method.PUT);

            var response = await SingletonClient.GetClient().Execute(request.request.AddJsonBody(_manufacturer));
            if (response.IsSuccess)
            {
                await Navigation.PushAsync(new TabsPage(_user));
            }


        }
    }
}

