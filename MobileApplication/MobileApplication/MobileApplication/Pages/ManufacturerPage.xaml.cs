﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApplication.Models;
using MobileApplication.Services;
using RestSharp.Portable;
using Xamarin.Forms;

namespace MobileApplication.Pages
{
    public partial class ManufacturerPage : ContentPage
    {
        private ListView listView;
        private User _user;
        private ICollection<Manufacturer> _manufacturer;

        protected override async void OnAppearing()
        {
            var request = new Rest(Globals.Global.apiManufacturer, Method.GET);
            var response = await SingletonClient.GetClient().Execute<ICollection<Manufacturer>>(request.request);
            _manufacturer = response.Data;
            foreach (var manufacturer in _manufacturer)
            {
                Application.Current.Properties.Add(manufacturer.ManfacturerName, manufacturer);
            }
            await Application.Current.SavePropertiesAsync();
            listView.ItemsSource = response.Data;
            base.OnAppearing();
        }

        public ManufacturerPage(User user)
        {
            this._user = user;
            var createProduct = new Button()
            { WidthRequest = 50, HeightRequest = 80, BackgroundColor = Color.FromHex("77D065"), Text = "Create New Manufacturer", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)) };
            createProduct.Clicked += CreateProductOnClicked;
            Padding = new Thickness(20, Device.OnPlatform(20, 20, 20), 20, 20);

            
            listView = new ListView
            {
                HasUnevenRows = true,
                // Define template for displaying each item.
                // (Argument of DataTemplate constructor is called for 
                //      each item; it must return a Cell derivative.)
                ItemTemplate = new DataTemplate(() =>
                {
                    // Create views with bindings for displaying each property.
                    var nameLabel = new Label() { HeightRequest = 20 };
                    nameLabel.SetBinding(Label.TextProperty, "ManfacturerName");
                    nameLabel.GestureRecognizers.Add(new TapGestureRecognizer { Command = new Command<Label>(OnLabelClicked), CommandParameter = nameLabel, NumberOfTapsRequired = 1 });

                    

                    // Return an assembled ViewCell.
                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.Center,
                            Spacing = -7,
                            Children =
                                {
                                    new Label() { HeightRequest = 20},
                                    nameLabel,
                                    new Label() { HeightRequest = 20},
                                    new Label() { HeightRequest = 20},
                                    new Label() {HeightRequest = 30}

                                }
                        }
                    };
                })
            };


            // Build the page.
            this.Content = new StackLayout
            {
                Children =
                {
                    new Label() {HeightRequest = 20},
                    listView,
                    createProduct
                }
            };
        }

        async void OnLabelClicked(Label clickedLabel)
        {
            var temp = clickedLabel.BindingContext as Manufacturer;

            await Navigation.PushAsync(new ManufacturerDetails(temp, _user));
        }

        async void CreateProductOnClicked(object sender, EventArgs eventArgs)
        {
            await Navigation.PushAsync(new ManufacturerCreation(_user));
        }
    }
    
}
