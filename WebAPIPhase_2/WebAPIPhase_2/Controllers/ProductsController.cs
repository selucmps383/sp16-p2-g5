﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPIPhase_2.Authentication;
using WebAPIPhase_2.Models;
using WebAPIPhase_2.Models.DTOs;
using WebAPIPhase_2.Services;

namespace WebAPIPhase_2.Controllers
{
    public class ProductsController : BaseAPIController
    {
     
        private WebAPIPhase_2Context db = new WebAPIPhase_2Context();
        private IProductRepository repo = new ProductRepository();

    
        public HttpResponseMessage GetProducts()
        {
            //  if (IsAuthorized(Request, new List<Role> { Role.Admin }))
            //{
            List<ProductDTO> ProductList = new List<ProductDTO>();

            foreach (var item in repo.GetAllProducts())
            {
                ProductList.Add(TheDTOFactory.Create(item));
            }

            return Request.CreateResponse(HttpStatusCode.OK, ProductList);


            // }
            // return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public HttpResponseMessage GetProduct(int id)
        {

            // find the product from the repo + database 
            Product productFound = repo.getProductById(id);

            if (productFound == null)
            {

                return Request.CreateResponse(HttpStatusCode.NotFound, "Product not found");

            }

            ProductDTO productfactoried = TheDTOFactory.Create(productFound);
            return Request.CreateResponse(HttpStatusCode.OK, productfactoried);

        }
      //  [HttpPost]
        [AllowAnonymous]
        [System.Web.Http.ActionName("UpdateProducts")]
        public HttpResponseMessage UpdateProducts([FromUri]string email, List<Product> products )
        {

            ProductPurchased purchased = new ProductPurchased();
            Product product;
            decimal count = 0;
            if (products == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,ModelState);
            }
            Sale sale = new Sale();
            sale.SaleDate = DateTime.Today;
            sale.Email = email;
            db.Sales.Add(sale);
            foreach (var items in products)
            {
                sale.TotalAmount += items.InventoryCount * items.Price;
            }
            foreach (var item in products)
            {
                product = db.Products.Find(item.ProductId);
                product.InventoryCount = product.InventoryCount - item.InventoryCount;

                purchased.ProductId = item.ProductId;
                purchased.Quantity = item.InventoryCount;
                purchased.SaleId = sale.SaleId;
                db.ProductPurchased.Add(purchased);

                try
                {
                    db.SaveChanges();

                    sendEmail(email, products);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(item.ProductId))
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }



        

        [HttpPut]
        [AllowAnonymous]
        [System.Web.Http.ActionName("PutProduct")]
        public HttpResponseMessage PutProduct( [FromBody]Product product)
        {
            Product productFound = repo.getProductById(product.ProductId);

            if (ModelState.IsValid)
            {

                if (productFound == null)
                {

                    repo.createProduct(product);

                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, product);
                    response.Headers.Location = new Uri(Url.Link("DefaultApi", new {id = product.ProductId}));


                    ProductDTO productfactoried = TheDTOFactory.Create(product);

                    return Request.CreateResponse(HttpStatusCode.OK, productfactoried);
                }
                else
                {
                    repo.putProduct(product.ProductId, product);
                    return Request.CreateResponse(HttpStatusCode.Accepted, product);
                }

            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }



        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ProductId == id) > 0;
        }

        private bool sendEmail(String email, List<Product> products)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(email);
                mail.From = new MailAddress("w0573147@selu.edu");
                mail.Subject = "Hello";
                string[] fsa = getName(products);


                int count = 1;
                foreach (Product product in products)
                {
                    mail.Body += " **Product " + count + "  Name: " +"  " + product.Name + "**  Quantity:  " + product.InventoryCount + " **Price:   " + product.Price + "   "+ "\r\n" + Environment.NewLine;
                    count++;
                   // mail.Body = print(product.Name);
                }

                
                    
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("convergent.origin@gmail.com", "Dangtest78");// Enter senders User name and password
                smtp.EnableSsl = true;
                Console.WriteLine("Sending email .... ");
                smtp.Send(mail);
                return true;
            }
            catch (Exception e)
            {
            //    Console.WriteLine("Email sending failed: " + e.Message);
            return false;
            }

          //  Console.WriteLine("Email has been sent to " + email);
            //Console.ReadKey();
        }

        private string[] getName(List<Product> products)
        {
            string[] name = new string[products.Count];
            for (int i = 0; i < products.Count; i++)
            {
                name[i] = products[i].Name;
                Console.WriteLine(name[i]);
            }
            return name;
        }
       

    }
}