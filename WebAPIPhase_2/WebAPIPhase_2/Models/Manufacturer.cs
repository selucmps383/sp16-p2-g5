﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIPhase_2.Models
{   [DataContract]
    public class Manufacturer
    {
        [Key]
        [DataMember]
        public int ManufacturerId { get; set; }

        [DataMember]
        public string ManfacturerName { get; set; }

        [JsonIgnore]
        public virtual ICollection<Product> Products { get; set; }
    }
}
